# Blog App

A demo project for the Intro  to Laravel Short Course.

## Getting Started
- Clone The project from Gitlab into your Machine
    ```bash
    $ git clone <project_url>
    ```
- Navigate through the root directory of the project.
    ```bash
    $ cd blog_app
    ```
- Download and Install all requried packages and dependecies.
    ```bash
    $ composer update
    $ npm install
    ```
- Create a copy of `.env.sample` to `.env` file.
    ```language
    $ cat .env.example > .env
    ```
- Create a new application key.
    ```bash
    $ php artisan key generate
    ```
- Update the `.env` file of the database configuration.
     ```
    DB_DATABASE=blog_app_db
    ```
- Create a new `blog_app_db` in `phpMyAdmin` client.    

    **NOTE:** XAMPP Control Panel must run the `apache` and `MySql` modules.

- Run the  database migrations
    ```bash
    $ php artisan migrate
    ```
- Run the applications server and build tools for the client
     ```bash
     $ npm run watch-poll
    $ php artisan serve
    ```
---

Created By KulasNgPinas