<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/posts/create', 'PostController@create');

Route::post('/posts', 'PostController@store');

Route::get('/posts', 'PostController@index');

Route::get('/posts/my-posts', 'PostController@myPosts');

Route::get('/posts/{id}', 'PostController@show');

Route::get('/posts/{id}/edit', 'PostController@edit');

Route::put('/posts/{id}', 'PostController@updatePost');

Route::put('/posts/{id}', 'PostController@archivedPost');

Route::delete('/posts/{id}', 'PostController@deletePost');

Route::put('/posts/{post_id}/{user_id}/like', 'PostController@like');

Route::put('/posts/{post_id}/{user_id}/dislike', 'PostController@dislike');
