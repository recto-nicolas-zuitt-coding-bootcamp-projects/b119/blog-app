@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <h1>Display All post</h1>
        </div>
        <div>
            <a href="/posts/create" class="btn btn-primary mb-5">Add Post</a>
        </div>
        @foreach( $posts as $post)
            <div class="row">
                <div class="card mb-5" style="width:50%">
                    <div class="card-body">
                        <h3 class="card-title">
                        <a href="/posts/{{$post->id}}">{{ $post->title }}</a>
                        </h3>
                        <p>{{ $post->content}}</p>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection