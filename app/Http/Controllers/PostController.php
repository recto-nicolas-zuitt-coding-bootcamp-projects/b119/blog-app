<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post; // model
use Auth;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')-> except(['index','show']);
    }
    
    public function create()
    {
        return view('posts.create');
    }

    public function store(Request $req)
    {
       $new_post = new Post([
           'title' => $req->input('title'),
           'content' => $req->input('content'),
           'isActive' => true,
           'user_id' => Auth::user()->id
       ]);

       $new_post->save();
       return redirect('/posts');
    }

    public function index()
    {
      $allPosts = Post::all()->where('isActive', true);

      return view('posts.index')->with('posts', $allPosts);
    }

    public function show($id)
    {
      $post = Post::find($id);

      return view('posts.show')->with('post', $post);
    }

    public function myPosts()
    {
      $user_posts = Auth::user()->posts;
      
      return view('posts.index')->with('posts', $user_posts);
    }

    public function edit($id)
    {
      $post = Post::find($id);
      
      return view('posts.edit')->with('post', $post);
    }

    public function updatePost($id, Request $req)
    {
      $post = Post::find($id);
      
      $post->title = $req->input('title');
      $post->content = $req->input('content');

      $post->save();

      return redirect("/posts/$id");
    }

    public function deletePost($id)
    {
      $post = Post::find($id);
      
      $post->delete();

      return redirect("/posts");
    }

    public function archivedPost($id)
    {
      $post = Post::find($id);
      
      $post->isActive = false;
      
      $post->save();
      return redirect("/posts");
    }

    public function like($post_id, $user_id)
    {
        $post = Post::find($post_id);
        $post->likes()->attach($user_id);
        return redirect("/posts/$post_id");
    }

    public function dislike($post_id, $user_id)
    {
        $post = Post::find($post_id);
        $post->likes()->detach($user_id);
        return redirect("/posts/$post_id");
    }

}
